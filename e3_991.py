import argparse
import logging
import sys
from typing import NoReturn

import gitlab
from gitlab.v4.objects import Project

from e3.git.registry import WrapperRegistry


def protect_branch(
    project: Project,
    branch: str,
    allowed_to_push: int = gitlab.DEVELOPER_ACCESS,
    allowed_to_merge: int = gitlab.DEVELOPER_ACCESS,
    simulate: bool = True,
) -> NoReturn:
    """Protect branch.

    :param project: affected gitlab project/repository
    :param branch: branch name or wildcard
    :param allowed_to_push: minimum gitlab access level required to push
    :param allowed_to_merge: minimum gitlab access level required to merge
    :param simulate: dry-run option
    """

    if simulate:
        logging.debug(f"Branch '{branch}' of {project.path} would have been protected")
    else:
        try:
            project.protectedbranches.delete(branch)
        except gitlab.exceptions.GitlabDeleteError:
            pass
        else:
            logging.debug(
                f"Existing protected '{branch}' in {project.path} unprotected"
            )
        finally:
            logging.debug(f"Protecting the branch '{branch}'")
            reply = project.protectedbranches.create(
                {
                    "name": branch,
                    "allowed_to_push": [{"access_level": allowed_to_push}],
                    "allowed_to_merge": [{"access_level": allowed_to_merge}],
                }
            )
            logging.debug(reply)


def protect_projects_in_group(
    group_id: int,
    token: str,
    dry_run: bool,
    debug: bool,
):
    """Protect projects in a group id."""
    if debug:
        logging.basicConfig(level=logging.DEBUG)


    registry = WrapperRegistry(top_group=group_id, private_token=token)

    group = registry._gl.groups.get(group_id)
    print(f"Protecting projects in group {group_id}: {group.name}")

    for proj_name, id_ in registry._projects.items():
        project = registry.get_project(id_)
        print(f"  Protecting project {project.path} ")

        allow_self_approve = True

        group_name = project.namespace["name"]
        if group_name in set(('core', 'communication')):
            allow_self_approve = False

        p_mras = project.approvals.get()
        p_mras.approvals_before_merge = 1
        p_mras.merge_requests_author_approval = allow_self_approve
        p_mras.save()

        protect_branch(
            project,
            branch=project.default_branch,
            allowed_to_push=gitlab.MAINTAINER_ACCESS,
            allowed_to_merge=gitlab.DEVELOPER_ACCESS,
            simulate=dry_run,
        )

    print(f"Successfully protected projects in {group_id}")


def main():
    """Run the main function."""
    parser = argparse.ArgumentParser(
        description="Script that protects projects belonging to a specified group"
    )
    parser.add_argument("--token", required=True, help="Gitlab token")

    parser.add_argument(
        "-n", "--dry-run", action="store_true", help="simulate action"
    )
    parser.add_argument(
        "-g",
        "--group_id",
        required=True,
        help="Top level group to search for repositories in",
    )
    parser.add_argument("--debug", action="store_true", help=argparse.SUPPRESS)
    args = parser.parse_args()

    protect_projects_in_group(**vars(args))


if __name__ == "__main__":
    if sys.version_info < (3, 6, 0):
        sys.stderr.write("You need python 3.6 or later to run this script\n")
        sys.exit(1)

    main()
